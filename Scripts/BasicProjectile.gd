extends KinematicBody2D
export var Speed:int
export var Angle: float
export var targetVelocity:Vector2
export var Damage = 5

func _physics_process(delta):
	#var direction = Vector2(1, 0)
	var velocity = Vector2(cos(Angle), sin(Angle))
	var movement = Speed * velocity * delta
	var collision = move_and_collide(movement)
	if collision:
		print(collision.collider)
		if collision.collider != null:
			if collision.collider.has_method("hit"):
				collision.collider.hit(self)
			queue_free()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
