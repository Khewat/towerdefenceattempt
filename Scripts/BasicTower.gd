extends KinematicBody2D
const BasicProjectile = preload("res://Scenes/BasicProjectile.tscn")
var muzzle: bool
var target
signal shoot
var rot_offset = 1.5

func shoot():
	if muzzle == true:
		emit_signal("shoot", $RightMuzzle.global_position, global_rotation - rot_offset) #+ $LeftMuzzle.global_position.angle_to(target.position))
		muzzle = false
	else:
		emit_signal("shoot", $LeftMuzzle.global_position, global_rotation - rot_offset)
		muzzle = true

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_instance_valid(target) :
		global_rotation += rot_offset + get_angle_to(target.global_position)

func _on_Timer_timeout():
	if target != null:
		shoot()
		
func _on_Range_body_entered(body):
	var groups = body.get_groups()
	print(groups)
	print(groups.find("enemies"))
	if groups.find("enemies") != -1:	
		target = body
	
func _on_Range_body_exited(body):
	if body == target:
		target = null
