extends VBoxContainer
var money_label 
signal build_tower_request
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	money_label = get_node("MarginContainer/HBoxContainer2/Bars/Bar/Count/Background/MoneyNumber")
	money_label.set_text("0")
	pass # Replace with function body.

func update_money(amount):
	money_label.set_text(amount as String)
	
func _on_BasicTowerButton_button_down():
	emit_signal("build_tower_request", 10)
