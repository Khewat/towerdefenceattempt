using Godot;
using System;
public class Main : Node2D
{
  PackedScene Mob = (PackedScene)ResourceLoader.Load("res://Scenes/Mob.tscn");
  PackedScene BasicTower = (PackedScene)ResourceLoader.Load("res://Scenes/BasicTower.tscn");
  PackedScene BasicProjectile = (PackedScene)ResourceLoader.Load("res://Scenes/BasicProjectile.tscn");

  public int Money
  {
    get{ return _money; }
    set {
      _money = value;
      GUI.Call("update_money", _money);
    }
  }
  private int _money = 0;
  VBoxContainer GUI;
  Position2D mobSpawn;
  bool towerPlacementMode = false;
  bool spawnMobs;
  int mobsRemaining = 0;

  public override void _Ready()
  {
    mobSpawn = GetNode<Position2D>("MobSpawn");
    GUI = (VBoxContainer)GetNode("GUI");
    GD.Print("ready");
    createTower(new Vector2(100, 100));
    spawnMob();
  }

  public void _shoot(Vector2 startLocation, float angleToTarget)
  {
    KinematicBody2D shot = (KinematicBody2D)BasicProjectile.Instance();
    shot.GlobalPosition = startLocation;
    shot.Set("Angle", angleToTarget);
    shot.Set("Speed", 500);
    AddChild(shot);
  }

  public void _mob_death(int value)
  {
    Money += value;
  }
  public void createTower(Vector2 position)
  {
    KinematicBody2D firstTower = (KinematicBody2D)BasicTower.Instance();
    firstTower.GlobalPosition = position;
    firstTower.Connect("shoot", this, nameof(_shoot));
    AddChild(firstTower);
  }

  public void spawnMob()
  {
    Node2D mob = (Node2D)Mob.Instance();
    mob.GlobalPosition = mobSpawn.GlobalPosition;
    mob.AddToGroup("enemies");
    mob.Set("Health", 10);
    mob.Connect("mob_death", this, nameof(_mob_death));
    AddChild(mob);
  }

  void _on_GUI_build_tower_request(int tower_cost)
  {
    if (tower_cost <= Money)
    {
      towerPlacementMode = true;
      Money -= tower_cost;
    }
    else
    {
      GD.Print("notenoughmoney");
    }
  }

  void _on_MobSpawnTimer_timeout(){
    GD.Print(mobsRemaining);
    if (mobsRemaining > 0){
      spawnMob();
      mobsRemaining -= 1;
    }
  }

  public override void _Input(InputEvent @event)
  {
    if (@event is InputEventMouseButton mouseEvent && mouseEvent.Pressed)
    {
      if ((ButtonList)mouseEvent.ButtonIndex == ButtonList.Left)
      {
        GD.Print($"Left button was clicked at {mouseEvent.Position}");
        if (towerPlacementMode)
        {
          createTower(mouseEvent.Position);
          towerPlacementMode = false;
        }
      }
    }
  }

}