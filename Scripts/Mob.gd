extends KinematicBody2D
signal mob_death
const score = 10 # Dollars for killing the mob
export var Velocity: Vector2
export var Health: int
export var speed = 15

func _ready():
	pass # Replace with function body.

func hit(projectile):
	print("mob hit by ", projectile)
	Health -= projectile.Damage
	if Health < 1:
		emit_signal("mob_death", score)
		queue_free()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	Velocity = Vector2(1,0)
	var movement = speed * Velocity * delta
	move_and_collide(movement)
